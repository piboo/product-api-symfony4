<?php

namespace App\Tests\Controller;

use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;


class ProductControllerTest extends WebTestCase
{

    protected static function getKernelClass()
    {
        return \App\Kernel::class;
    }

    private function loadOneProduct(): ReferenceRepository {
        return $this->loadFixtures([
            \App\DataFixtures\LoadProductTestData::class
        ])->getReferenceRepository();

    }

    public function testNoProductDetail()
    {

        $this->loadOneProduct();

        $client = $this->makeClient();

        $client->request('GET', '/products/1');


        $this->assertEquals(404, $client->getResponse()->getStatusCode());

    }

    public function testGetProductDetail()
    {

        $this->loadOneProduct();
        $client = $this->makeClient();

        $client->request('GET', '/products/aaaaaaaa-bbbb-1234-1234-aabbccddeeff');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }

    public function testCreateProduct()
    {

        $client = $this->makeClient();

        $content = '{
            "name": "GBP", 
            "price": 11.1,
            "description": "New Publisher"
        }';


        $client->request(
            'POST',
            '/products',
            array(),
            array(),
            array(
                'CONTENT_TYPE'          => 'application/json'
            ),
            $content
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertNotNull( $client->getResponse()->headers->get('location'));
    }

    public function testWrongRequestCreateProduct()
    {

        $client = $this->makeClient();

        $content = '{
            "name": "GBP", 
            "price": 11.1
        }';


        $client->request(
            'POST',
            '/products',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json'
            ),
            $content
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testUpdateProduct()
    {

        $this->loadOneProduct();
        $client = $this->makeClient();

        $content = '{
            "name": "GBP", 
            "price": 11.1,
            "description": "New Publisher 2"
        }';


        $client->request(
            'POST',
            '/products/aaaaaaaa-bbbb-1234-1234-aabbccddeeff',
            array(),
            array(),
            array(
                'CONTENT_TYPE'          => 'application/json'
            ),
            $content
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotNull( $client->getResponse()->headers->get('location'));
    }
}