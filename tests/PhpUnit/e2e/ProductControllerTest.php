<?php

namespace App\Tests\PhpUnit\e2e;


use Opis\JsonSchema\{
    Validator
};
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    /** @var Validator */
    private $validator;

    protected static function getKernelClass()
    {
        return \App\Kernel::class;
    }

    private function loadOneProduct(): ReferenceRepository {
        return $this->loadFixtures([
            \App\DataFixtures\LoadProductTestData::class
        ])->getReferenceRepository();

    }

    public function setUp()
    {
        parent::setUp();
        $this->validator = new Validator();
        $this->loadOneProduct();
    }

    public function testProductListResponse()
    {
        $schema = json_decode(
            file_get_contents('./tests/jsonSchema/basic-response-json-schema.json', true)
        );

        $client = $this->makeClient();

        $client->request(
            'GET',
            '/products',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'ACCEPT' => 'application/json'
            )
            );

        $content = new \SimpleXMLElement($client->getResponse()->getContent());

        $content = json_decode(json_encode($content));

        $result = $this->validator->dataValidation($content, $schema);

        $this->assertFalse($result->hasErrors());
    }

    public function testProductResponse()
    {
        $schema = json_decode(
            file_get_contents('./tests/jsonSchema/basic-response-json-schema.json', true)
        );

        $client = $this->makeClient();

        $client->request(
            'GET',
            '/products',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'ACCEPT' => 'application/json'
            )
        );

        $content = new \SimpleXMLElement($client->getResponse()->getContent());

        $content = json_decode(json_encode($content));

        $result = $this->validator->dataValidation($content, $schema);

        $this->assertFalse($result->hasErrors());
    }
}