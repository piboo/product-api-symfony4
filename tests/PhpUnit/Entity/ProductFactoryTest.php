<?php

namespace App\Tests\Entity;

use App\DataTransport\Request\ProductRequestDT;
use App\Entity\ProductFactory;
use PHPUnit\Framework\TestCase;

class ProductFactoryTest extends TestCase
{

    /** @var ProductFactory */
    private $productFactory;

    public function setUp() {

        parent::setUp();

        $uuidGenerator = $this->getMockBuilder('\App\Service\UuidGenerator')
            ->getMock();

        $uuidGenerator->expects($this->once())
            ->method('generateUuid')
            ->will($this->returnValue('uuid'));

        $this->productFactory = new ProductFactory($uuidGenerator);

    }

    public function testBuildProduct() {

        $productDT = new ProductRequestDT();
        $productDT->setName('name');
        $productDT->setDescription('description');
        $productDT->setPrice(11.11);

        $product  = $this->productFactory->build(
            $productDT
        );

        $this->assertEquals($productDT->getName(), $product->getName());
        $this->assertEquals('uuid',$product->getUuid());
        $this->assertEquals($productDT->getDescription(),$product->getDescription());
        $this->assertEquals($productDT->getPrice(),$product->getPrice());
    }
}