<?php

namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service\UuidGenerator;

class UuidGeneratorTest extends TestCase
{


    public function testValidUuid(){
        $uuidGenerator = new UuidGenerator();

        $uuid = $uuidGenerator->generateUuid();

        $UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';

        $this->assertRegExp($UUIDv4,$uuid);
    }
}