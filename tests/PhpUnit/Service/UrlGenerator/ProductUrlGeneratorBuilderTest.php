<?php

namespace App\Tests\Service\UrlGenerator;

use App\Entity\Product;
use App\Service\UrlGenerator\ProductUrlGeneratorBuilder;
use App\Service\UrlGenerator\UrlGenerator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Router;


class ProductUrlGeneratorBuilderTest extends TestCase
{

    protected function getRouterMock():Router
    {
        $mock =  $this->getMockBuilder('\Symfony\Bundle\FrameworkBundle\Routing\Router')
            ->disableOriginalConstructor()
            ->setMethods(['generate'])
            ->getMockForAbstractClass();


        $mock->expects($this->any())
            ->method('generate')
            ->with('product_get',[
                'uuid' => 'aaaaaaaa-bbbb-1234-1234-aabbccddeeff'
            ])
            ->will($this->returnValue('https://www.product.com/aaaaaaaa-bbbb-1234-1234-aabbccddeeff'));

        return $mock;
    }

    protected function getProduct(): Product
    {
        $mock =  $this->getMockBuilder('\App\Entity\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getUuid'])
            ->getMockForAbstractClass();

        $mock->expects($this->any())
            ->method('getUuid')
            ->will($this->returnValue('aaaaaaaa-bbbb-1234-1234-aabbccddeeff'));
        return $mock;
    }

    public function testValidUuid(){
        $productUrlGeneratorBuilder = new ProductUrlGeneratorBuilder($this->getRouterMock());

        $urlGenerator = $productUrlGeneratorBuilder->build($this->getProduct());

        $this->assertInstanceOf(UrlGenerator::class, $urlGenerator);
        $this->assertEquals(
            'https://www.product.com/aaaaaaaa-bbbb-1234-1234-aabbccddeeff',
            $urlGenerator->getUrl()
        );
    }

}