<?php

namespace App\Tests\Service;

use App\Exception\ParameterNotFoundException;
use App\Service\UrlGenerator\UrlGenerator;
use PHPUnit\Framework\TestCase;



class ProductGeneratorTest extends TestCase
{

    protected function getRouterMock()
    {
        $mock =  $this->getMockBuilder('\Symfony\Bundle\FrameworkBundle\Routing\Router')
            ->disableOriginalConstructor()
            ->setMethods(['generate'])
            ->getMockForAbstractClass();


        $mock->expects($this->any())
            ->method('generate')
            ->with('url',[
                'uuid' => 'aaaaaaaa-bbbb-1234-1234-aabbccddeeff'
            ])
            ->will($this->returnValue('https://www.product.com/aaaaaaaa-bbbb-1234-1234-aabbccddeeff'));

        return $mock;
    }

    protected function getProduct()
    {
        $mock =  $this->getMockBuilder('\App\Entity\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getUuid'])
            ->getMockForAbstractClass();

        $mock->expects($this->any())
            ->method('getUuid')
            ->will($this->returnValue('aaaaaaaa-bbbb-1234-1234-aabbccddeeff'));
        return $mock;
    }

    public function testValidUuid(){
        $productUrlGenerator = new UrlGenerator($this->getRouterMock());

        $productUrlGenerator->setRouteName('url');
        $productUrlGenerator->setAccessibleExternallyObject($this->getProduct());

        $url = $productUrlGenerator->getUrl();



        $this->assertEquals('https://www.product.com/aaaaaaaa-bbbb-1234-1234-aabbccddeeff',$url);
    }

    public function testEmptyUrlParameter(){
        $this->expectException(ParameterNotFoundException::class);

        $productUrlGenerator = new UrlGenerator($this->getRouterMock());

        $productUrlGenerator->setRouteName('');
        $productUrlGenerator->setAccessibleExternallyObject($this->getProduct());

        $url = $productUrlGenerator->getUrl();
    }


    public function testEmptyUrlProduct(){
        $this->expectException(ParameterNotFoundException::class);

        $productUrlGenerator = new UrlGenerator($this->getRouterMock());

        $mock =  $this->getMockBuilder('\App\Entity\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getUuid'])
            ->getMockForAbstractClass();

        $mock->expects($this->any())
            ->method('getUuid')
            ->will($this->returnValue(''));

        $productUrlGenerator->setRouteName('');
        $productUrlGenerator->setAccessibleExternallyObject($mock);

        $url = $productUrlGenerator->getUrl();

        $this->assertEquals('https://www.product.com/aaaaaaaa-bbbb-1234-1234-aabbccddeeff',$url);
    }
}