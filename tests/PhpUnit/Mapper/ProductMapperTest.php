<?php

namespace App\Tests\Mapper;

use App\DataTransport\Request\ProductRequestDT;
use App\Entity\Product;
use App\Entity\ProductFactory;
use App\Exception\DBException;
use App\Mapper\ProductMapper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\TestCase;


class ProductMapperTest extends TestCase
{

    /** @var EntityRepository */
    private $productRepo;

    /** @var Product */
    private $product;

    /** @var ProductFactory */
    private $productFactory;

    /** @var EntityManager */
    private $objectManager;

    public function setUp() {

        parent::setUp();

        $this->product = new Product();

        $this->product->setName('test product');
        $this->product->setDescription('test description');
        $this->product->setPrice(11.11);
        $this->product->setUuid('aaaaaaaa-bbbb-1234-1234-aabbccddeeff');

        $this->productRepo = $this->getMockBuilder('\Doctrine\ORM\EntityRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->productFactory = $this->getMockBuilder(ProductFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->objectManager = $this->createMock(EntityManager::class);

        $this->objectManager->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($this->productRepo));

    }


    public function testReturnOneProduct(){


        $this->productRepo->expects($this->once())
            ->method('findOneBy')
            ->with(['uuid'=>'aaaaaaaa-bbbb-1234-1234-aabbccddeeff'])
            ->will($this->returnValue($this->product));

        $productMapper = new ProductMapper(
            $this->objectManager,
            $this->productFactory
        );

        $product = $productMapper->getProductByUuid('aaaaaaaa-bbbb-1234-1234-aabbccddeeff');

        $this->assertNotEmpty($product);

        $this->assertEquals($product->getUuid(), 'aaaaaaaa-bbbb-1234-1234-aabbccddeeff');
    }

    public function testReturnNoProduct(){

        $this->productRepo->expects($this->once())
            ->method('findOneBy')
            ->with(['uuid'=>'xxxxxxxx-bbbb-1234-1234-aabbccddeeff'])
            ->will($this->returnValue(null));

        $productMapper = new ProductMapper(
            $this->objectManager,
            $this->productFactory
        );

        $product = $productMapper->getProductByUuid('xxxxxxxx-bbbb-1234-1234-aabbccddeeff');

        $this->assertEmpty($product);

    }

    public function testCreateProduct(){

        $productDT = new ProductRequestDT();
        $productDT->setName('name');
        $productDT->setDescription('description');
        $productDT->setPrice(11.11);

        $productMapper = new ProductMapper(
            $this->objectManager,
            $this->productFactory
        );

        $product = $productMapper->createProduct($productDT);

        $this->assertNotEmpty($product);

    }

    public function testCreateProductException()
    {
        $this->expectException(DBException::class);

        $productDT = new ProductRequestDT();
        $productDT->setName('name');
        $productDT->setDescription('description');
        $productDT->setPrice(11.11);

        $productMapper = new ProductMapper(
            $this->objectManager,
            $this->productFactory
        );

        $this->objectManager->expects($this->any())
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $product = $productMapper->createProduct($productDT);
    }

    public function testUpdateProduct(){

        $productDT = new ProductRequestDT();
        $productDT->setName('name new');
        $productDT->setDescription('description new');
        $productDT->setPrice(11.11);

        $product = new Product();
        $product->setUuid("uuid");

        $productMapper = new ProductMapper(
            $this->objectManager,
            $this->productFactory
        );

        $product = $productMapper->updateProduct($product, $productDT);

        $this->assertNotEmpty($product);

        $this->assertEquals('name new',$product->getName());
        $this->assertEquals('description new',$product->getDescription());
        $this->assertEquals(11.11,$product->getPrice());

    }

    public function testUpdateProductException()
    {
        $this->expectException(DBException::class);

        $productDT = new ProductRequestDT();
        $productDT->setName('name');
        $productDT->setDescription('description');
        $productDT->setPrice(11.11);

        $product = new Product();
        $product->setUuid("uuid");

        $productMapper = new ProductMapper(
            $this->objectManager,
            $this->productFactory
        );

        $this->objectManager->expects($this->any())
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $productMapper->updateProduct($product, $productDT);
    }
}