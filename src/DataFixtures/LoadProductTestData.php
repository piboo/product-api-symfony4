<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadProductTestData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $product = new Product();

        $product->setName('test product');

        $product->setDescription('test description');

        $product->setPrice(11.11);

        $product->setUuid('aaaaaaaa-bbbb-1234-1234-aabbccddeeff');

        $manager->persist($product);
        $manager->flush();

        $this->addReference('test-product', $product);
    }

    public function getOrder()
    {
        return 1;
    }
}