<?php

namespace App\DataTransport;

use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all");
 */
class ListDT
{
    /**
     * @Serializer\Expose
     * @Serializer\Groups({"detail"})
     *
     * @var int
     */
    private $count;

    /** @var array */
    private $list;

    /**
     * ListDT constructor.
     * @param int $count
     * @param array $list
     */
    public function __construct(int $count, array $list)
    {
        $this->count = $count;
        $this->list = $list;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }






}