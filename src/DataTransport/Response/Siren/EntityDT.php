<?php

namespace App\DataTransport\Response\Siren;

use App\DataTransport\Response\SerializableResponseInterface;
use Doctrine\Common\Collections\ArrayCollection;

use JMS\Serializer\Annotation as Serializer;

class EntityDT implements SerializableResponseInterface
{

    /**
     * @Serializer\Expose
     */
    private $classes;

    /**
     * @Serializer\Expose
     */
    private $properties;

    /**
     * @Serializer\Type("ArrayCollection<App\DataTransport\Response\Siren\SubEntityDT>")
     * @Serializer\Expose
     */
    private $entities;

    /**
     * @Serializer\Type("ArrayCollection<App\DataTransport\Response\Siren\LinkDT>")
     * @Serializer\Expose
     */
    private $links;

    /**
     * @Serializer\Type("ArrayCollection<App\DataTransport\Response\Siren\ActionDT>")
     * @Serializer\Expose
     */
    private $actions;

    /**
     * EntityDT constructor.
     * @param string[] $classes
     * @param $properties
     * @param ArrayCollection $entities
     * @param ArrayCollection $actions
     * @param ArrayCollection $links
     */
    public function __construct(
        array $classes,
        array $properties,
        ArrayCollection $entities,
        ArrayCollection $actions,
        ArrayCollection $links
    ){
        $this->classes = $classes;
        $this->properties = $properties;
        $this->entities = $entities;
        $this->actions = $actions;
        $this->links = $links;
    }



}
