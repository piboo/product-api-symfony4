<?php

namespace App\DataTransport\Response\Siren;

class LinkDTFactory
{

    /**
     * @param string[] $rel
     * @param string $href
     * @return LinkDT
     */
    public function create(
        array $rel,
        string $href
    ): LinkDT {

        return new LinkDT(
            $rel,
            $href
        );
    }

}