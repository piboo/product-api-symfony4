<?php

namespace App\DataTransport\Response\Siren;

class ActionDTFactory
{

    public function create(
        string $name,
        array $class,
        string $method,
        string $url
    ): ActionDT {

        return new ActionDT(
            $name,
            $class,
            $method,
            $url
        );
    }

}