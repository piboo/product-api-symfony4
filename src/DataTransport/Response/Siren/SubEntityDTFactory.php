<?php

namespace App\DataTransport\Response\Siren;

use Doctrine\Common\Collections\ArrayCollection;

class SubEntityDTFactory
{

    /**
     * @param array $classes
     * @param array $properties
     * @param ArrayCollection $actions
     * @param ArrayCollection $links
     * @return SubEntityDT
     */
    public function create(
        array $classes,
        array $properties,
        ArrayCollection $actions,
        ArrayCollection $links
    ): SubEntityDT {

        return new SubEntityDT(
            $classes,
            $properties,
            $actions,
            $links
        );
    }

}