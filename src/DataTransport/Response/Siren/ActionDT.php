<?php

namespace App\DataTransport\Response\Siren;


class ActionDT
{
    /** @var string */
    private $name;

    /** @var string[] */
    private $class;

    /** @var string */
    private $method;

    /** @var string */
    private $href;

    /**
     * ActionDT constructor.
     * @param string $name
     * @param array $class
     * @param string $method
     * @param string $href
     */
    public function __construct(
        string $name,
        array $class,
        string $method,
        string $href
    ){
        $this->name = $name;
        $this->class = $class;
        $this->method = $method;
        $this->href = $href;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string[]
     */
    public function getClass(): array
    {
        return $this->class;
    }

    /**
     * @param string[] $class
     */
    public function setClass(array $class): void
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @param string $href
     */
    public function setHref(string $href): void
    {
        $this->href = $href;
    }


}