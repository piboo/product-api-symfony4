<?php

namespace App\DataTransport\Response\Siren;

use Doctrine\Common\Collections\ArrayCollection;

class EntityDTFactory
{

    /**
     * @param array $classes
     * @param array $properties
     * @param ArrayCollection $entities
     * @param ArrayCollection $actions
     * @param ArrayCollection $links
     * @return EntityDT
     */
    public function create(
        array $classes,
        array $properties,
        ArrayCollection $entities,
        ArrayCollection $actions,
        ArrayCollection $links
    ): EntityDT {

        return new EntityDT(
            $classes,
            $properties,
            $entities,
            $actions,
            $links
        );
    }

}