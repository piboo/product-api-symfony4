<?php

namespace App\DataTransport\Response\Siren;

class LinkDT
{
    /** @var string[] */
    private $rel;

    /** @var string */
    private $href;


    /**
     * LinkDT constructor.
     * @param string[] $rel

     * @param string $href
     */
    public function __construct(
        array $rel,
        string $href
    ) {
        $this->rel = $rel;
        $this->href = $href;
    }

    /**
     * @return string[]
     */
    public function getRel(): array
    {
        return $this->rel;
    }

    /**
     * @param string[] $rel
     */
    public function setRel(array $rel): void
    {
        $this->rel = $rel;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @param string $href
     */
    public function setHref(string $href): void
    {
        $this->href = $href;
    }


}
