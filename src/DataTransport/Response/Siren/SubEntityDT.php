<?php

namespace App\DataTransport\Response\Siren;

use App\DataTransport\Response\SerializableResponseInterface;
use Doctrine\Common\Collections\ArrayCollection;

use JMS\Serializer\Annotation as Serializer;

class SubEntityDT implements SerializableResponseInterface
{

    /**
     * @Serializer\Expose
     */
    private $classes;

    /**
     * @Serializer\Expose
     */
    private $properties;

    /**
     * @Serializer\Type("ArrayCollection<App\DataTransport\Response\Siren\LinkDT>")
     * @Serializer\Expose
     */
    private $links;

    /**
     * @Serializer\Type("ArrayCollection<App\DataTransport\Response\Siren\ActionDT>")
     * @Serializer\Expose
     */
    private $actions;

    /**
     * EntityDT constructor.
     * @param array $classes
     * @param $properties
     * @param ArrayCollection $actions
     * @param ArrayCollection $links
     */
    public function __construct(
        array $classes,
        $properties,
        ArrayCollection $actions,
        ArrayCollection $links
    ){
        $this->classes = $classes;
        $this->properties = $properties;
        $this->actions = $actions;
        $this->links = $links;
    }


}
