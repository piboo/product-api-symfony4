<?php

namespace App\EventListener;

use App\Exception\ValidationException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        $message = [
            "msg" =>$exception->getMessage(),
            "code" =>$exception->getCode()
        ];


        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent(json_encode($message));

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } elseif ($exception instanceof ValidationException) {
            /** @var ConstraintViolationListInterface $validationErrors */
            $validationErrors = $exception->getValidationErrors();
            $arrayMsg = [];
            for ($i = 0; $i < $validationErrors->count(); $i++) {
                $arrayMsg[] = ["field" => $validationErrors->get($i)->getPropertyPath(),
                    "error" => $validationErrors->get($i)->getMessage()];
            }
            $response->setContent(json_encode($arrayMsg));
            $response->setStatusCode(400);
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $event->setResponse($response);
        return;
    }
}