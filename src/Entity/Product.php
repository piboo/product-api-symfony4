<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="product")
 *
 * @Serializer\ExclusionPolicy("all");
 */
class Product implements AccessibleExternallyInterface {
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Groups({"list", "detail"})
     *
     * @ORM\Column(
     *  length=36,
     *  options={"fixed"=true},
     *  type="string",
     *  unique=true
     * )
     */
    private $uuid;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Groups({"list", "detail"})
     *
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @var float
     *
     * @Serializer\Expose
     * @Serializer\Groups({"detail"})
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Groups({"detail"})
     *
     * @ORM\Column(type="text")
     */
    private $description;


    public function getId(): int
    {
        return $this->id;
    }


    public function getUuid(): string
    {
        return $this->uuid;
    }


    public function setUuid(string $uuid): Product
    {
        $this->uuid = $uuid;

        return $this;
    }


    public function getName(): string
    {
        return $this->name;
    }


    public function setName(string $name): Product
    {
        $this->name = $name;

        return $this;
    }


    public function getPrice(): float
    {
        return $this->price;
    }


    public function setPrice(float $price): Product
    {
        $this->price = $price;

        return $this;
    }


    public function getDescription(): string
    {
        return $this->description;
    }


    public function setDescription(string $description): Product
    {
        $this->description = $description;

        return $this;
    }



}