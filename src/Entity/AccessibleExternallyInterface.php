<?php

namespace App\Entity;


interface AccessibleExternallyInterface
{
    public function getUuid();
}