<?php


namespace App\Entity;

use App\DataTransport\Request\ProductRequestDT;
use App\Service\UuidGenerator;


class ProductFactory
{

    /** @var UuidGenerator */
    private $uuidGenerator;

    public function __construct(UuidGenerator $uuidGenerator) {

        $this->uuidGenerator = $uuidGenerator;
    }

    public function build(
        ProductRequestDT $productRequestDT
    ): Product {

        $product = new Product();

        $product
            ->setName($productRequestDT->getName())
            ->setUuid($this->uuidGenerator->generateUuid())
            ->setDescription($productRequestDT->getDescription())
            ->setPrice($productRequestDT->getPrice());

        return $product;
    }

}