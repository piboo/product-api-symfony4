<?php

namespace App\Controller;

use App\DataTransport\Request\ProductRequestDT;
use App\Exception\DBException;
use App\Exception\ValidationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductsController extends FOSRestController
{

    /**
     * @Rest\Post("/products", name="product_post")
     *
     * @ParamConverter("productRequestDT",
     *     class="App\DataTransport\Request\ProductRequestDT",
     *     converter="fos_rest.request_body")
     *
     * @param ProductRequestDT $productRequestDT
     * @param ConstraintViolationListInterface $validationErrors
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws ValidationException,HttpException
     * @throws HttpException
     */
    public function createProduct(
        ProductRequestDT $productRequestDT,
        ConstraintViolationListInterface $validationErrors
    ) {
        if ($validationErrors->count() > 0) {

            throw new ValidationException($validationErrors);
        }

        $productMapper = $this->get('mapper.product');

        try {

            $product = $productMapper->createProduct($productRequestDT);

        } catch (DBException $e) {

            throw new HttpException(
                500,
                'DB error inserting product:'.$e->getMessage().' code'.$e->getCode()
            );
        }

        $urlGeneratorBuilder = $this->get('service.url_generator.product');
        $url = $urlGeneratorBuilder->build($product)->getUrl();

        $view = View::create()
            ->setStatusCode(201)
            ->setHeader("location",$url);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/products/{uuid}", name="product_get")
     *
     * @param string $uuid
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProductDetailAction(string $uuid) {

        $productMapper = $this->get('mapper.product');
        $product = $productMapper->getProductByUuid($uuid);

        if(!$product){
            throw new NotFoundHttpException( "ProductRequestDT not found.");;
        }

        $responseBuilder = $this->get('service.response.siren.product.response_factory');

        $response = $responseBuilder->createResponse($product);

        $view = View::create()
            ->setStatusCode(200)
            ->setData($response);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/products/{uuid}", name="product_update_post")
     *
     * @ParamConverter("productRequestDT",
     *     class="App\DataTransport\Request\ProductRequestDT",
     *     converter="fos_rest.request_body")
     *
     * @param string $uuid
     * @param ProductRequestDT $productRequestDT
     * @param ConstraintViolationListInterface $validationErrors
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws ValidationException,HttpException
     * @throws HttpException
     */
    public function updateProduct(
        string $uuid,
        ProductRequestDT $productRequestDT,
        ConstraintViolationListInterface $validationErrors
    ) {
        if ($validationErrors->count() > 0) {

            throw new ValidationException($validationErrors);
        }

        $productMapper = $this->get('mapper.product');

        $product = $productMapper->getProductByUuid($uuid);

        if(!$product) {
            throw new BadRequestHttpException("Product not Found");
        }

        try {

            $product = $productMapper->updateProduct(
                $product,
                $productRequestDT
            );

        } catch (DBException $e) {

            throw new HttpException(
                500,
                'DB error update product:'.$e->getMessage().' code'.$e->getCode()
            );
        }

        $urlGeneratorBuilder = $this->get('service.url_generator.product');
        $url = $urlGeneratorBuilder->build($product)->getUrl();

        $view = View::create()
            ->setStatusCode(200)
            ->setHeader("location",$url);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/products", name="product_list")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllProducts() {

        $productMapper = $this->get('mapper.product');
        $product = $productMapper->getAllProducts();

        $responseBuilder = $this->get('service.response.siren.list.response_factory');

        $response = $responseBuilder->createResponse($product);

        $view = View::create()
            ->setStatusCode(200)
            ->setData($response);

        return $this->handleView($view);
    }

}