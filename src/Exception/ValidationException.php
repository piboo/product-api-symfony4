<?php

namespace App\Exception;


use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{

    /** @var ConstraintViolationListInterface  */
    private $validationErrors;

    public function __construct(ConstraintViolationListInterface $validationErrors)
    {
        $this->validationErrors = $validationErrors;
        parent::__construct("Validation Errors", 400);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }


}