<?php

namespace App\Exception;


use Symfony\Component\HttpKernel\Exception\HttpException;

class ParameterNotFoundException extends HttpException
{
    public function __construct(string $parameter, string $class)
    {
        $msg = 'Parameter '.$parameter. ' not Found in class: '. $class;

        parent::__construct(500, $msg );
    }
}