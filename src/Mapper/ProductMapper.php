<?php

namespace App\Mapper;

use App\DataTransport\ListDT;
use App\DataTransport\Request\ProductRequestDT;
use App\Entity\Product;
use App\Entity\ProductFactory;
use App\Exception\DBException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class ProductMapper
{
    /** @var EntityManager */
    private $entityManager;

    /** @var ProductFactory */
    private $productFactory;

    public function __construct(
        EntityManager $objectManager,
        ProductFactory $productFactory
    ) {
        $this->entityManager = $objectManager;
        $this->productFactory = $productFactory;
    }

    /**
     * @param $uuid
     * @return Product|null
     */
    public function getProductByUuid($uuid): ?Product  {

        $productRepository = $this->entityManager->getRepository('App:Product');

        /** @var Product $product */
        $product = $productRepository->findOneBy(['uuid'=>$uuid]);

        return $product;
    }

    /**
     * @param ProductRequestDT $productRequestDT
     * @return Product
     * @throws DBException
     */
    public function createProduct(ProductRequestDT $productRequestDT):Product {

        $product =$this->productFactory->build(
            $productRequestDT
        );

        try {

            $this->entityManager->persist($product);
            $this->entityManager->flush();

        } catch (ORMException $exception) {

            throw new DBException(
                $exception->getMessage(),
                $exception->getCode()
            );
        }
        return $product;
    }

    /**
     * @param Product $product
     * @param ProductRequestDT $productRequestDT
     * @return Product
     * @throws DBException
     */
    public function updateProduct(
        Product $product,
        ProductRequestDT $productRequestDT
    ):Product {

        $product->setName(
            $productRequestDT->getName()
        );

        $product->setDescription(
            $productRequestDT->getDescription()
        );

        $product->setPrice(
            $productRequestDT->getPrice()
        );

        try {

            $this->entityManager->persist($product);
            $this->entityManager->flush();

        } catch (ORMException $exception) {

            throw new DBException(
                $exception->getMessage(),
                $exception->getCode()
            );
        }
        return $product;
    }

    /**
     * @return ListDT
     */
    public function getAllProducts():ListDT {

        $productRepository = $this->entityManager->getRepository('App:Product');

        /** @var array $products */
        $products = $productRepository->findAll();

        $listDT = new ListDT(
            count($products),
            $products
        );

        return $listDT;
    }
}