<?php

namespace App\Service\Response\Siren\ProductList;


use App\DataTransport\ListDT;
use App\DataTransport\Response\Siren\SubEntityDTFactory;
use App\Service\Response\Siren\SirenEntitiesFactoryInterface;
use App\Service\Response\Siren\SirenLinksFactoryInterface;
use App\Service\Response\Siren\SirenObjectClassInterface;
use App\Service\Response\Siren\SirenPropertiesExtractorInterface;
use Doctrine\Common\Collections\ArrayCollection;

class ProductListEntitiesFactory implements SirenEntitiesFactoryInterface
{

    /** @var SirenObjectClassInterface */
    private $sirenObjectClass;

    /** @var SirenPropertiesExtractorInterface */
    private $sirenPropertiesExtractor;

    /** @var SirenLinksFactoryInterface */
    private $sirenLinkFactory;

    /** @var SubEntityDTFactory */
    private $subEntityDTFactory;

    /**
     * ProductListEntitiesFactory constructor.
     * @param SirenObjectClassInterface $sirenObjectClass
     * @param SubEntityDTFactory $subEntityDTFactory
     * @param SirenPropertiesExtractorInterface $sirenPropertiesExtractor
     * @param SirenLinksFactoryInterface $sirenLinkFactory
     */
    public function __construct(
        SirenObjectClassInterface $sirenObjectClass,
        SubEntityDTFactory $subEntityDTFactory,
        SirenPropertiesExtractorInterface $sirenPropertiesExtractor,
        SirenLinksFactoryInterface $sirenLinkFactory
    ){
        $this->sirenObjectClass = $sirenObjectClass;
        $this->subEntityDTFactory = $subEntityDTFactory;
        $this->sirenPropertiesExtractor = $sirenPropertiesExtractor;
        $this->sirenLinkFactory = $sirenLinkFactory;
    }

    /**
     * @param ListDT $objectToSerialize
     * @return ArrayCollection
     */
    public function createEntities($objectToSerialize): ArrayCollection {

        $arrayEntities = new ArrayCollection();
        foreach ($objectToSerialize->getList() as $product) {
            $arrayEntities->add(
                $this->subEntityDTFactory->create(
                    $this->sirenObjectClass->getClass(),
                    $this->sirenPropertiesExtractor->getProperties($product,'list'),
                    new ArrayCollection(),
                    $this->sirenLinkFactory->createLinks($product)
                )
            );
        }

        return $arrayEntities;
    }
}