<?php

namespace App\Service\Response\Siren\ProductList;

use App\Service\Response\Siren\SirenObjectClassInterface;

class ProductListClassObject implements SirenObjectClassInterface
{
    public function getClass(): array
    {
        return ['list','product'];
    }
}