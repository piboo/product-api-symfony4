<?php

namespace App\Service\Response\Siren;


use Doctrine\Common\Collections\ArrayCollection;

interface SirenLinksFactoryInterface
{
    public function createLinks($objectToSerialize):ArrayCollection;
}