<?php

namespace App\Service\Response\Siren\Product;


use App\DataTransport\Response\Siren\ActionDT;
use App\DataTransport\Response\Siren\ActionDTFactory;
use App\Entity\Product;
use App\Service\Response\Siren\SirenActionsFactoryInterface;
use App\Service\UrlGenerator\ProductUrlGeneratorBuilder;
use Doctrine\Common\Collections\ArrayCollection;

class ProductActionsFactory implements SirenActionsFactoryInterface
{

    /** @var ProductUrlGeneratorBuilder */
    private $productUrlGeneratorBuilder;

    /** @var ActionDTFactory */
    private $actionDTFactory;

    /**
     * ProductActionsFactory constructor.
     * @param ActionDTFactory $actionDTFactory
     * @param ProductUrlGeneratorBuilder $productUrlGeneratorBuilder
     */
    public function __construct(
        ActionDTFactory $actionDTFactory,
        ProductUrlGeneratorBuilder $productUrlGeneratorBuilder
    ){
        $this->actionDTFactory = $actionDTFactory;
        $this->productUrlGeneratorBuilder = $productUrlGeneratorBuilder;
    }

    /**
     * @param $product
     * @return ArrayCollection
     * @throws \Exception
     */
    public function createActions($product): ArrayCollection {

        if (get_class($product)!= Product::class ) {
            throw new \Exception("Wrong Object ".get_class($product)."passed to ".self::class);
        }

        $arrayActions = new ArrayCollection();

        $actionDT = $this->actionDTFactory->create(
            'updateProduct',
            ['product'],
            'POST',
            $this->productUrlGeneratorBuilder->build($product)->getUrl()
        );

        $arrayActions->add($actionDT);

        return $arrayActions;

    }

}