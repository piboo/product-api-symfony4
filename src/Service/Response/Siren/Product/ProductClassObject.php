<?php

namespace App\Service\Response\Siren\Product;

use App\Service\Response\Siren\SirenObjectClassInterface;

class ProductClassObject implements SirenObjectClassInterface
{
    public function getClass(): array
    {
        return ['product'];
    }
}