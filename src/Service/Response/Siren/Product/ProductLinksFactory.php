<?php

namespace App\Service\Response\Siren\Product;

use App\DataTransport\Response\Siren\LinkDTFactory;
use App\Entity\Product;
use App\Service\Response\Siren\SirenLinksFactoryInterface;
use App\Service\UrlGenerator\ProductUrlGeneratorBuilder;
use Doctrine\Common\Collections\ArrayCollection;

class ProductLinksFactory implements SirenLinksFactoryInterface
{

    /** @var ProductUrlGeneratorBuilder */
    private $productUrlGeneratorBuilder;

    /** @var LinkDTFactory */
    private $linkDTFactory;

    /**
     * ProductLinksFactory constructor.
     * @param LinkDTFactory $linkDTFactory
     * @param ProductUrlGeneratorBuilder $productUrlGeneratorBuilder
     */
    public function __construct(
        LinkDTFactory $linkDTFactory,
        ProductUrlGeneratorBuilder $productUrlGeneratorBuilder
    ){
        $this->linkDTFactory = $linkDTFactory;
        $this->productUrlGeneratorBuilder = $productUrlGeneratorBuilder;
    }

    /**
     * @param $product
     * @return ArrayCollection
     * @throws \Exception
     */
    public function createLinks($product): ArrayCollection {

        if (get_class($product)!= Product::class ) {
            throw new \Exception("Wrong Object ".get_class($product)."passed to ".self::class);
        }

        $arrayLinks = new ArrayCollection();

        $linkDT = $this->linkDTFactory->create(
            ['self'],
            $this->productUrlGeneratorBuilder->build($product)->getUrl()
        );

        $arrayLinks->add($linkDT);

        return $arrayLinks;
    }

}