<?php

namespace App\Service\Response\Siren;


use Doctrine\Common\Collections\ArrayCollection;

interface SirenEntitiesFactoryInterface
{
    public function createEntities($objectToSerialize):ArrayCollection;
}