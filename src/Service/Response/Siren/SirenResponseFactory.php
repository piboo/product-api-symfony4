<?php

namespace App\Service\Response\Siren;

use App\DataTransport\Response\Siren\EntityDT;
use App\DataTransport\Response\Siren\EntityDTFactory;

class SirenResponseFactory
{
    /** @var SirenObjectClassInterface */
    private $sirenObjectClass;

    /** @var SirenPropertiesExtractorInterface */
    private $sirenPropertiesExtractor;

    /** @var SirenEntitiesFactoryInterface */
    private $sirenEntitiesFactory;

    /** @var SirenActionsFactoryInterface */
    private $sirenActionsFactory;

    /** @var SirenLinksFactoryInterface */
    private $sirenLinkFactory;

    /** @var EntityDTFactory */
    private $entityDTFactory;

    /**
     * SirenResponseFactory constructor.
     * @param EntityDTFactory $entityDTFactory
     * @param SirenObjectClassInterface $sirenObjectClass
     * @param SirenPropertiesExtractorInterface $sirenPropertiesExtractor
     * @param SirenEntitiesFactoryInterface $sirenEntitiesFactory
     * @param SirenActionsFactoryInterface $sirenActionsFactory
     * @param SirenLinksFactoryInterface $sirenLinkFactory
     */
    public function __construct(
        EntityDTFactory $entityDTFactory,
        SirenObjectClassInterface $sirenObjectClass,
        SirenPropertiesExtractorInterface $sirenPropertiesExtractor,
        SirenEntitiesFactoryInterface $sirenEntitiesFactory,
        SirenActionsFactoryInterface $sirenActionsFactory,
        SirenLinksFactoryInterface $sirenLinkFactory
    ){
        $this->sirenObjectClass = $sirenObjectClass;
        $this->entityDTFactory = $entityDTFactory;
        $this->sirenPropertiesExtractor = $sirenPropertiesExtractor;
        $this->sirenEntitiesFactory = $sirenEntitiesFactory;
        $this->sirenActionsFactory = $sirenActionsFactory;
        $this->sirenLinkFactory = $sirenLinkFactory;
    }


    /**
     * @param $objectToSerialize
     * @return EntityDT
     */
    public function createResponse($objectToSerialize) {

        return $this->entityDTFactory->create(
            $this->sirenObjectClass->getClass(),
            $this->sirenPropertiesExtractor->getProperties($objectToSerialize,'detail'),
            $this->sirenEntitiesFactory->createEntities($objectToSerialize),
            $this->sirenActionsFactory->createActions($objectToSerialize),
            $this->sirenLinkFactory->createLinks($objectToSerialize)
        );
    }
}