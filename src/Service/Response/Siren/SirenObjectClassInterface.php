<?php

namespace App\Service\Response\Siren;


interface SirenObjectClassInterface
{
    public function getClass():array;
}