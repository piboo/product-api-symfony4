<?php

namespace App\Service\Response\Siren;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class PropertiesExtractor implements SirenPropertiesExtractorInterface
{
    /** @var  Serializer */
    private $serializer;

    /**
     * UserPropertiesBuilder constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $object
     * @param string $serializationGroup
     * @return array|mixed
     */
    public function getProperties(
        $object,
        $serializationGroup = "list"
    ) : array {
        return $this->serializer->toArray(
            $object,
            SerializationContext::create()->setGroups($serializationGroup)->setSerializeNull(true)
        );

    }
}