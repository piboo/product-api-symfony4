<?php

namespace App\Service\Response\Siren;

use Doctrine\Common\Collections\ArrayCollection;

class EmptyResponseElement implements SirenActionsFactoryInterface,SirenEntitiesFactoryInterface,SirenLinksFactoryInterface
{

    public function createEntities($objectToSerialize) : ArrayCollection
    {
        return new ArrayCollection();
    }

    public function createActions($objectToSerialize):ArrayCollection
    {
        return new ArrayCollection();
    }

    public function createLinks($objectToSerialize) :ArrayCollection
    {
        return new ArrayCollection();
    }
}