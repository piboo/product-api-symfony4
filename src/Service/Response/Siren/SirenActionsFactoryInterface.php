<?php

namespace App\Service\Response\Siren;


use Doctrine\Common\Collections\ArrayCollection;

interface SirenActionsFactoryInterface
{
    public function createActions($objectToSerialize):ArrayCollection;
}