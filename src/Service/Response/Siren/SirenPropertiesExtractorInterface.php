<?php

namespace App\Service\Response\Siren;


interface SirenPropertiesExtractorInterface
{
    public function getProperties(
        $objectToSerialize,
        $serializationGroup
    ):array;

}