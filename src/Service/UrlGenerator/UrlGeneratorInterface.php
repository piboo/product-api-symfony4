<?php

namespace App\Service\UrlGenerator;


interface UrlGeneratorInterface
{
    public function getUrl(): String;
}