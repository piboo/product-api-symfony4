<?php

namespace App\Service\UrlGenerator;

use App\Entity\Product;
use Symfony\Component\Routing\Router;

class ProductUrlGeneratorBuilder
{
    /** @var Router */
    private $router;

    /**
     * UrlGenerator constructor.
     * @param Router $router
     */
    public function __construct(
        Router $router
    ){
        $this->router = $router;
    }

    public function build($product):UrlGeneratorInterface {

        $urlGenerator = new UrlGenerator($this->router);
        $urlGenerator->setRouteName('product_get');
        $urlGenerator->setAccessibleExternallyObject($product);

        return $urlGenerator;
    }

}