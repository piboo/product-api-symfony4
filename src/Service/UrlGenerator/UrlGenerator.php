<?php

namespace App\Service\UrlGenerator;

use App\Entity\AccessibleExternallyInterface;
use App\Exception\ParameterNotFoundException;
use Symfony\Component\Routing\Router;


class UrlGenerator implements UrlGeneratorInterface {

    /** @var Router */
    private $router;

    /** @var string */
    private $routeName;

    /** @var AccessibleExternallyInterface */
    private $accessibleExternallyObject;

    /**
     * UrlGenerator constructor.
     * @param Router $router
     */
    public function __construct(
        Router $router
    ){
        $this->router = $router;
    }

    /**
     * @param string $routeName
     */
    public function setRouteName(string $routeName): void
    {
        $this->routeName = $routeName;
    }

    /**
     * @param AccessibleExternallyInterface $accessibleExternallyObject
     */
    public function setAccessibleExternallyObject(AccessibleExternallyInterface $accessibleExternallyObject): void
    {
        $this->accessibleExternallyObject = $accessibleExternallyObject;
    }


    public function getUrl(): String {

        if (empty($this->routeName)){
            throw new ParameterNotFoundException('$routeName', get_class($this));
        }

        if (empty($this->accessibleExternallyObject->getUuid())){
            throw new ParameterNotFoundException('$accessibleExternally->getUuid()', get_class($this));
        }

        return $this->router->generate(
            $this->routeName, [
                'uuid' => $this->accessibleExternallyObject->getUuid()
            ]
        );
    }

}