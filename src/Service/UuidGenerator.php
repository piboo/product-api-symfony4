<?php

namespace App\Service;

use Ramsey\Uuid\Uuid;

class UuidGenerator
{

    public function generateUuid() : string {
       return  Uuid::uuid4();
    }

}
